import React from 'react'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addTask} from '../../actions/'
import { returnStatement } from '@babel/types';
 class Taskbar extends React.Component{
     render(){
         return(
             <div>
                <input type="text" ref="task" placeholder="add the todo"/>
                <button onClick={()=>this.props.addTask(this.refs.task.value)}>Add TODO</button>
             </div>
         );

     }

 };

 function mapDispatchToProps(dispatch){
  return bindActionCreators({addTask},dispatch); 
 }

 export default connect(()=>{},mapDispatchToProps)(Taskbar);