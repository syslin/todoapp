import React from 'react';
import { connect } from 'react-redux';
import Task from '../task/'
import { returnStatement } from '@babel/types';
class TaskList extends React.Component {
    constructor() {
        super();
        this.state = {
          task: ''
        }
      }
    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Tasks</th>
                        <th>Action</th>
                    </tr>
                </thead>
              
                {this.props.tasks.map((task, index) => <Task key={index} task={task} />)}
            
            </table>

        );

    }
}

const mapStateToProps=state=> {
    return  state;

}

export default connect(mapStateToProps)(TaskList);