import React from 'react';
import Header from './components/header/';
import Taskbar from './components/taskbar/';
import TaskList from './components/Tasklist/';


function App() {
  return (
    <div className="App">
      <Header/>
      <Taskbar/>
      <TaskList/>
    </div>
  );
}

export default App;
