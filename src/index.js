// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';
// import {Provider} from 'react-redux';
// import {createStore} from 'redux';
// import reducers from './reducers/';


// let store=createStore(reducers);
// ReactDOM.render(
//     <Provider store={store}>  
//         <App />
//     </Provider>
// , document.getElementById('root'));

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import rootReducer from './reducers'
import App from './components/App';
// import {reducer as formReducer} from 'redux-form'


// const rootReducer= combineReducers({
//   form:formReducer
// })
const store = createStore(rootReducer ,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
  

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
