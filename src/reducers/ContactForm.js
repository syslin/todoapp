import  React from 'react'
import {Field,reduxForm} from 'redux-form'

let ContactForm =props =>{
    const{handelSubmit}=props
    return <form onSubmit ={handleSubmit}>
        <div>
        <label htmlFor="firstName">First Name</label>
        <Field name="firstName" component="input" type="text" />
        </div>
        <button type="submit">Submit</button>
    </form>
}

ContactForm =reduxForm({
    form: 'contact'
})(ContactForm)

export default  ContactForm 